﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Hangman
{
    class Program
    {
        /// <summary>Returns true if the given letter is in the word.</summary>
        static bool inWord(string letter, string word)
        {
            return word.ToLower().Contains(letter);
        }

        /// <summary>Returns the number of times the given letter occurs in the word.</summary>
        static int timesInWord(string letter, string word)
        {
            return Regex.Matches(word, Regex.Escape(letter)).Count;
        }

        /// <summary>Prompts the user for a letter, returning it as a string.</summary>
        static string getLetter()
        {
            while (true)
            {
                Console.Write("Please enter a letter\n> ");
                var input = Console.ReadLine();

                if (input == null)
                {
                    Console.WriteLine("ERROR: Invalid input.");
                }
                else if (input.Length < 1)
                {
                    Console.WriteLine("ERROR: Empty input.");
                }
                else if (input.Length > 1)
                {
                    Console.WriteLine("ERROR: Too many characters.");
                }
                else if (!char.IsLetter(input[0]))
                {
                    Console.WriteLine("ERROR: Not a letter.");
                }
                else
                {
                    return input.ToLower();
                }
            }
        }

        /// <summary>Prints the word, replacing letters not in guesses with underscores.</summary>
        static void printWord(string word, List<string> guesses)
        {
            foreach (var letter in word)
            {
                Console.Write(guesses.Contains(letter.ToString()) ? letter : '_');
            }
            Console.Write('\n');
        }

        /// <summary>Returns true if every letter in word has been guessed</summary>
        static bool checkWin(string word, List<string> guesses)
        {
            int letterCount = 0;
            foreach(var guess in guesses)
            {
                letterCount += timesInWord(guess, word);
            }

            return letterCount == word.Length;
        }

        static void Main(string[] args)
        {
            // 10 random words from randomwordgenerator.com
            var words = new List<string>{ "reform", "heel", "mail", "fair", "enthusiasm", "spot", "reluctance", "fortune", "pneumonia", "contrast" };
            var random = new Random();

            while (true)
            {
                string word = words[random.Next(words.Count)];
                var guesses = new List<string>();
                var incorrectGuesses = new List<string>();
                int chancesLeft = 7;

                while (true)
                {
                    printWord(word, guesses);
                    Console.WriteLine($"Chances left: {chancesLeft}");

                    string letter = getLetter();
                    if (guesses.Contains(letter) || incorrectGuesses.Contains(letter))
                    {
                        Console.WriteLine($"You already guessed {letter}. Try again.");
                        continue;
                    }
                    if (inWord(letter, word))
                    {
                        guesses.Add(letter);
                        if (checkWin(word, guesses))
                        {
                            Console.WriteLine($"You win! The word was {word}. You had {chancesLeft} chances left.");
                            break;
                        }    
                    }
                    else
                    {
                        incorrectGuesses.Add(letter);
                        chancesLeft -= 1;
                        if (chancesLeft < 0)
                        {
                            Console.WriteLine($"You lose. The word was {word}.");
                            break;
                        }
                    }
                }

                Console.Write("\nType q to quit, or anything else to play again.\n> ");
                if (Console.ReadLine() == "q")
                {
                    break;
                }
            }
        }
    }
}
